<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('post');
});


Route::get('/post', 'PostController@index');
Route::get('/post/tambah', 'PostController@tambah');
Route::post('/post/store', 'PostController@store');
Route::get('/post/edit/{id}', 'PostController@edit');
Route::put('/post/update/{id}', 'PostController@update');
Route::get('/post/hapus/{id}', 'PostController@delete');