<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>CRUD Eloquent Laravel</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                     <a>CRUD Daftar Pertanyaan</a>
                </div>
                <div class="card-body">
                    <a href="/post/tambah" class="btn btn-primary">Input Data Pertanyaan Baru</a>
                    <br/>
                    <br/>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Isi</th>
                                <th>Tanggal Dibuat</th>
                                <th>Tanggal Diperbarui</th>
                                <th>Jawaban Tepat</th>
                                <th>Penanya</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($post as $p)
                            <tr>
                                <td>{{ $p->judul }}</td>
                                <td>{{ $p->isi }}</td>
                                <td>{{ $p->tanggal_dibuat }}</td>
                                <td>{{ $p->tanggal_diperbarui }}</td>
                                <td>{{ $p->jawaban_tepat }}</td>
                                <td>{{ $p->profil_id }}</td>
                                <td>
                                    <a href="/post/edit/{{ $p->id }}" class="btn btn-warning">Edit</a>
                                    <a href="/post/hapus/{{ $p->id }}" class="btn btn-danger">Hapus</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>