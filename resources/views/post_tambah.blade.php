<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>+</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    <strong>TAMBAH DATA</strong> 
                </div>
                <div class="card-body">
                    <a href="/post" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    
                    <form method="post" action="/post/store">
 
                        {{ csrf_field() }}
 
                        <div class="form-group">
                            <label>Judul Pertanyaan</label>
                            <input type="text" name="judul" class="form-control" placeholder="...">
 
                            @if($errors->has('judul'))
                                <div class="text-danger">
                                    {{ $errors->first('judul')}}
                                </div>
                            @endif
 
                        </div>
 
                        <div class="form-group">
                            <label>Isi</label>
                            <textarea name="isi" class="form-control" placeholder="..."></textarea>
 
                             @if($errors->has('isi'))
                                <div class="text-danger">
                                    {{ $errors->first('isi')}}
                                </div>
                            @endif
 
                        </div>

                        <div class="form-group">
                            <label>Tanggal Dibuat</label>
                            <input type="date" name="tanggal_dibuat" class="form-control" >
 
                            @if($errors->has('tanggal_dibuat'))
                                <div class="text-danger">
                                    {{ $errors->first('tanggal_dibuat')}}
                                </div>
                            @endif
 
                        </div>

                        <div class="form-group">
                            <label>Tanggal Diperbarui</label>
                            <input type="date" name="tanggal_diperbarui" class="form-control">
 
                            @if($errors->has('tanggal_diperbarui'))
                                <div class="text-danger">
                                    {{ $errors->first('tanggal_diperbarui')}}
                                </div>
                            @endif
 
                        </div>

                        <div class="form-group">
                            <label>Jawaban Tepat</label>
                            <input type="text" name="jawaban_tepat" class="form-control" placeholder="...">
 
                            @if($errors->has('jawaban_tepat'))
                                <div class="text-danger">
                                    {{ $errors->first('jawaban_tepat')}}
                                </div>
                            @endif
 
                        </div>

                        
 
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
 
                    </form>
 
                </div>
            </div>
        </div>
    </body>
</html>