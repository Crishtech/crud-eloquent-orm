<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
 
use App\Post;
 
class PostController extends Controller
{
 
    public function index()
    {
    	$post = Post::all();
    	return view('post', ['post' => $post]);
    }
 
    public function tambah()
    {
    	return view('post_tambah');
    }
 
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'judul' => 'required',
            'isi' => 'required',
            'tanggal_dibuat' => 'required',
            'tanggal_diperbarui' => 'required',
            'jawaban_tepat' => 'required',
            'profil_id' => 'required'
    	]);
 
        Post::create([
    		'judul' => $request->judul,
            'isi' => $request->isi,
            'tanggal_dibuat' => $request->tanggal_dibuat,
            'tanggal_diperbarui' => $request->tanggal_diperbarui,
            'jawaban_tepat' => $request->jawaban_tepat,
            'profil_id' => $request->profil_id
    	]);
 
    	return redirect('/post');
    }

    public function edit($id)
    {
        $post = Post::find($id);
    return view('post_edit', ['post' => $post]);
    }
    
    public function update($id, Request $request)
    {
        $this->validate($request,[
            'judul' => 'required',
            'isi' => 'required',
            'tanggal_dibuat' => 'required',
            'tanggal_diperbarui' => 'required',
            'jawaban_tepat' => 'required',
            'profil_id' => 'required'
        ]);
    
        $post = Post::find($id);
        $post->judul = $request->judul;
        $post->isi = $request->isi;
        $post->tanggal_ibuat = $request->tanggal_dibuat;
        $post->tanggal_diperbarui = $request->tanggal_diperbarui;
        $post->jawaban_tepat = $request->jawaban_tepat;
        $post->save();
        return redirect('/post');
    }

    public function delete($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect('/post');
    }
}